var Graphs = (function() {

    var _ = {};

    _.colors = { W : "rgb(255, 255, 225)", U : "rgb(160, 221, 249)", B : "rgb(196, 184, 182)", R : "rgb(246, 162, 139)", G : "rgb(140, 211, 170)", C : 'Gray' };

    var generic_options;

    var init = function() {
        generic_options = {
            tooltip : {
                trigger: 'axis',
                axisPointer : {         
                    type : 'shadow'      
                }
            },
            grid: {
                left: '10px',
                right: '10px',
                bottom: '10px',
                containLabel: true
            },
            yAxis : [
                {
                    type : 'value'
                }
            ],
        };
    }

    _.setCMCDistributionGraph = function(data) {
        var xAxis = Object.keys(data);
        var values = Object.values(data);

        var options = {
            color: ['#3398DB'],
            title: {
                text : 'Converted Mana Distribution'
            },
            xAxis : [
                {
                    type : 'category',
                    data : xAxis,
                    axisTick: {
                        alignWithLabel: true
                    }
                }
            ],
            series : [
                {
                    name:'Cards',
                    type:'bar',
                    barWidth: '60%',
                    data: values
                }
            ]
        }

        var chart_options = Object.assign({}, generic_options, options);

        var chart = echarts.init(document.getElementById('cmc_dist_chart'));

        chart.setOption(chart_options);
    }

    _.setCardTypeDistributionGraph = function(origData) {
        
        var keys = Object.keys(origData);

        var data = [];

        keys.forEach((k, i) => {
            data.push({name : k, value : origData[k].count});
        });

        var options = {
            title: {
                text : 'Card Type Distribution'
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'right',
                data: keys
            },
            series : [
                {
                    name:'Cards',
                    center: ['33%', '50%'],
                    type:'pie',
                    data: data
                }
            ]
        }

        var chart_options = Object.assign({}, options);

        var chart = echarts.init(document.getElementById('cardtype_dist_chart'));

        chart.setOption(chart_options);
    }

    _.setColorDistributionGraph = function(data) {

        var colors = _.colors;

        var keys = Object.keys(data);
        var values = Object.values(data);

        var data = [];

        keys.forEach((k, i ) => {
            var obj = {name : k, value : values[i] };
            if(colors[k]) {
                obj.itemStyle = {};
                obj.itemStyle.color = colors[k]
            }
            data.push(obj);
        });

        var options = {
            title: {
                text : 'Color Distribution'
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'right',
                data: keys
            },
            series : [
                {
                    name:'Cards',
                    center: ['33%', '50%'],
                    type:'pie',
                    data: data
                }
            ]
        }

        var chart_options = Object.assign({}, options);

        var chart = echarts.init(document.getElementById('color_dist_chart'));

        chart.setOption(chart_options);
    }

    _.setRarityDistributionGraph = function(data) {
        var colors = { rare : "Gold", mythic : "darkorange", uncommon : "Silver", common : "White", special : "Purple", basic : 'Brown' }

        var keys = Object.keys(data);
        var values = Object.values(data);

        var data = [];

        var orderedData = [];
        var orderedKeys = []; 
        var theOrder = ["basic", "common", "uncommon", "rare", "mythic", "special"];

        keys.forEach((k, i) => {
            var obj = {name : k, value : values[i] };
            if(colors[k]) {
                obj.itemStyle = {};
                obj.itemStyle.color = colors[k]
            }
            data.push(obj);
        });

        for (var i = 0; i < theOrder.length; i++) {
            var currentKey = theOrder[i];
            
            data.forEach(d => {
                if(d.name == currentKey) {
                    orderedKeys.push(currentKey);
                    orderedData.push(d);
                }
            });
        }

        var options = {
            title: {
                text : 'Card Rarity Distribution'
            },
            xAxis : [
                {
                    type : 'category',
                    data : orderedKeys,
                    axisTick: {
                        alignWithLabel: true
                    }
                }
            ],
            series : [
                {
                    name:'Cards',
                    type:'bar',
                    barWidth: '60%',
                    data: orderedData
                }
            ]
        }

        var chart_options = Object.assign({}, generic_options, options);

        var chart = echarts.init(document.getElementById('rarity_dist_chart'));

        chart.setOption(chart_options);
    }

    _.setDevotionDistributionGraph = function(data) {
        var colors = _.colors;

        var keys = Object.keys(data);
        var values = Object.values(data);

        var data = [];

        keys.forEach((k, i ) => {
            var obj = {name : k, value : values[i] };
            if(colors[k]) {
                obj.itemStyle = {};
                obj.itemStyle.color = colors[k]
            }
            data.push(obj);
        });

        var options = {
            title: {
                text : 'Devotion Distribution'
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'right',
                data: keys
            },
            series : [
                {
                    name:'Cards',
                    center: ['33%', '50%'],
                    type:'pie',
                    data: data
                }
            ]
        }

        var chart_options = Object.assign({}, options);

        var chart = echarts.init(document.getElementById('devotion_dist_chart'));

        chart.setOption(chart_options);
    }

    _.setLandInHandChanceDistribution = function(data) {
        var colors = _.colors;

        var keys = Object.keys(data);
        var values = Object.values(data);

        var data = [];

        keys.forEach((k, i ) => {
            var obj = {name : k, value : values[i] };
            if(colors[k]) {
                obj.itemStyle = {};
                obj.itemStyle.color = colors[k]
            }
            data.push(obj);
        });

        var options = {
            title: {
                text : 'Land Draw Chances'
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'right',
                data: keys
            },
            series : [
                {
                    name:'Lands',
                    center: ['33%', '50%'],
                    type:'pie',
                    data: data
                }
            ]
        }

        var chart_options = Object.assign({}, options);

        var chart = echarts.init(document.getElementById('landdraw_chance_chart'));

        chart.setOption(chart_options);
    }

    _.setLandManaGeneratingColorDistribution = function(data) {
        var colors = _.colors;

        var keys = Object.keys(data);
        var values = Object.values(data);

        var data = [];

        keys.forEach((k, i ) => {
            var obj = {name : k, value : values[i] };
            if(colors[k]) {
                obj.itemStyle = {};
                obj.itemStyle.color = colors[k]
            }
            data.push(obj);
        });

        var options = {
            title: {
                text : 'Land Mana Color Distribution'
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'right',
                data: keys
            },
            series : [
                {
                    name:'Mana',
                    center: ['33%', '50%'],
                    type:'pie',
                    data: data
                }
            ]
        }

        var chart_options = Object.assign({}, options);

        var chart = echarts.init(document.getElementById('manaproduce_dist_chart'));

        chart.setOption(chart_options);
    }

    init();

    return _;

})();