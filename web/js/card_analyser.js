// Deck with cards
var Deck = function (data) {
    var _ = {};

    // public properties
    _.cards = [];
    _.analyser;

    // initialization function
    var init = function (cardData) {
        _.cards = cardData;
        _.analyser = new Deck_Analyser(_);

        reloadAnalyzer();
    };

    // Add multiple cards to deck
    _.addCards = function (cards) {
        _.cards.push(...cards);
        reloadAnalyzer();
    };

    // Add single card to deck
    _.addCard = function (card) {
        _.cards.push(card);
        reloadAnalyzer();
    };

    // remove a card from the deck
    _.removeCard = function (card) {
        // TODO : remove a card

        reloadAnalyzer();
    };

    // Reload the analyzer based on the current deck
    var reloadAnalyzer = function () {
        _.analyser.reload();
    };

    // initialize the deck with the JSON data set
    init(data);

    // Return the scope object
    return _;
};

// Deck Analyzer
var Deck_Analyser = function (deck) {
    var _ = {};

    // Public properties
    _.deck;
    _.cardAmount;
    _.cards;
    _.nonLandCards;
    _.landCards;

    // Initialization
    var init = function (deckData) {
        _.deck = deckData;

        // clean variables
        _.cardAmount = 0;
        _.cards = [];
        _.nonLandCards = [];
        _.landCards = [];
    };

    // Reload the card piles in the deck
    _.reload = function () {

        // Check if deck is valid
        if (!_.deck || !_.deck.cards || !_.deck.cards.length) {
            console.log("Deck was not found or does not contain any cards!");
            return;
        }

        // Loop through all the cards in the deck
        _.deck.cards.forEach(cardData => {
            _.cardAmount += cardData.amount;

            // For each instance of a certain card ( 1 card can be in the deck multiple times )
            for (var x = 0; x < cardData.amount; x++) {
                
                // Collect every card in 'cards'
                if(cardData.card) {
                    _.cards.push(cardData.card);
    
                    // Split deck in land cards and non-land cards
                    if (cardData.card.types.indexOf("Land") === -1) {
                        _.nonLandCards.push(cardData.card);
                    }else {
                        _.landCards.push(cardData.card);
                    }
                }
            }
        });
    };

    // Get converted mana distribution from deck
    _.getConvertedManaDistribution = function () {
        var distribution = {};

        _.nonLandCards.forEach(card => {
            if (distribution[card.cmc]) {
                distribution[card.cmc]++;
            } else {
                distribution[card.cmc] = 1;
            }
        });

        return distribution;
    };

    // Get average converted mana cost of cards in deck
    _.getConvertedManaAverage = function () {
        var total = 0;

        _.nonLandCards.forEach(card => {
            total += card.cmc;
        });

        return total / _.nonLandCards.length;
    };

    // Get card type distribution in deck
    _.getCardTypeDistribution = function () {
        var types = {};

        _.cards.forEach(card => {
            var type = card.types.join(" ");
            if (types[type]) {
                types[type].count++;
                types[type].cards.push(card);
            } else {
                types[type] = { count : 1, cards : [card]};
            }
        });

        return types;
    };

    // Get rarity distribution in deck
    _.getRarityDistribution = function () {
        var rarities = {};

        _.cards.forEach(card => {
            var rarity = card.rarity;
            if (rarities[rarity]) {
                rarities[rarity]++;
            } else {
                rarities[rarity] = 1;
            }
        });

        return rarities;
    };

    // Get all creature cards
    _.getCreatureCards = function(cards) {
        var cards = cards || _.cards;

        var creatureCards = [];

        cards.forEach(card => {
            if(card.types.indexOf('Creature') !== -1) {
                creatureCards.push(card);
            }
        });

        return creatureCards;
    }

    // Get number of creature cards
    _.getNumberOfCreatureCards = function() {
        return _.getCreatureCards().length;
    }

    // Get all legendary cards in the deck
    _.getLegendaryCards = function(cards) {

        var cards = cards || _.cards;

        var legendaryCards = [];

        cards.forEach(card => {
            if (card.super_types.includes("Legendary")) {
                legendaryCards.push(card);
            }
        });

        return legendaryCards;
    }

    // Get the number of legendary cards
    _.getNumberOfLegendaryCards = function () {
        return _.getLegendaryCards().length;
    };

    // Get card color distribution in the deck
    _.getCardColorDistribution = function () {
        var colors = {};

        _.nonLandCards.forEach(card => {
            var ci;

            if (card.color_identity.length === 1) {
                ci = card.color_identity[0];
            } else if (card.color_identity.length > 1) {
                ci = card.color_identity.join("/");
            }

            if (ci) {
                if (colors[ci]) {
                    colors[ci]++;
                } else {
                    colors[ci] = 1;
                }
            }
        });

        return colors;
    };

    // Get the devotion distribution in the deck
    _.getDevotionDistribution = function () {
        var devotion = {};

        var colors = ['W', 'U', 'B', 'R', 'G', 'C'];

        _.nonLandCards.forEach(card => {
            if(card.mana_cost) {
                var coloredMana = card.mana_cost.replace(/[\W\d]/g, "").split("");
                coloredMana.forEach(m => {
    
                    // Filter non colored symbols
                    if(colors.indexOf(m) === -1) { return; }
    
                    if (devotion[m]) {
                        devotion[m]++;
                    } else {
                        devotion[m] = 1;
                    }
                });
            }
        });

        return devotion;
    };

    // Get the mana color distribution of land cards
    _.getLandManaGeneratingColorDistribution = function() {
        // Check what colors lands produce, and split it between
        var manaColorProduced = {};
        var colors = ['W', 'U', 'B', 'R', 'G', 'C'];
        var numbers = { 'one' : 1, 'two' : 2, 'three' : 3, 'four' : 4, 'five' : 5, 'six' : 6, 'seven' : 7, 'eight' : 8, 'nine' : 9, 'ten' : 10 };

        _.landCards.forEach(land => {

            var anyRegex = new RegExp(`(one|two|three|four|five|six|seven|eight|nine|ten) mana of any [one ]?color`, 'im');
            var anyMatch = anyRegex.exec(land.oracle_text);

            // check for any
            if(anyMatch && anyMatch.length > 1) {
                var index = anyMatch[1];
                var number = numbers[index];

                if(manaColorProduced['Any']) {
                    manaColorProduced['Any'] += number;
                }else{
                    manaColorProduced['Any'] = number;
                }
            }

            // Check for colors/colorless
            if(land.oracle_text){
                for(var ci in colors) {
                    var color = colors[ci];
                    var regex = new RegExp(`(?<color>\{${color}\})`, 'igm');
                    var count = (land.oracle_text.match(regex) || []).length;
    
                    if(count && manaColorProduced[color]) {
                        manaColorProduced[color]++;
                    }else if(count) {
                        manaColorProduced[color] = 1;
                    }
                }
            }else{
                land.color_identity.forEach(color => {
                    if(manaColorProduced[color]) {
                        manaColorProduced[color]++;
                    }else{
                        manaColorProduced[color] = 1;
                    }
                });
            }
        });

        return manaColorProduced;
    }

    // Helper function to do factorial calculations
    var factorial = function(num)
    {
        var rval=1;
        for (var i = 2; i <= num; i++) {
            rval = rval * i;
        }
        return rval;
    }

    // Get the chance to draw each number of land cards in the opening hand
    _.getLandInHandChanceDistribution = function (drawAmount) {
        drawAmount = drawAmount || 7;
        // Calculate the chance of hanving 0,1,2,3,4,5,6,7 lands in hand ( first draw )
        var chances = {};
        var amountOfLands = _.landCards.length;
        var amountOfNonLands = _.nonLandCards.length;
        var amountOfCards = _.cards.length;

        for(var l = 0; l <= drawAmount; l++) {

            var landChance = (factorial(amountOfLands) / (factorial(l) * factorial(amountOfLands - l)));
            var nonLandChance = (factorial(amountOfNonLands) / (factorial(drawAmount - l) * factorial(amountOfNonLands - (drawAmount - l))));

            var totalChance = (factorial(amountOfCards) / (factorial(drawAmount) * factorial(amountOfCards - drawAmount)));

            var chance = (landChance * nonLandChance) / totalChance;

            // var landChance = (l == 0) ? 1 : amountOfLands / l;
            // var nonLandChance = (l == 7) ? 1 : amountOfNonLands / (drawAmount - l);

            // var chance = (landChance * nonLandChance) / (100 / drawAmount);

            chances[l] = chance;
        }

        return chances;
    }

    // Get total number of cards in deck
    _.getTotalCardsCount = function() {
        return _.cards.length;
    }

    // Get total number of lands in deck
    _.getLandCardCount = function(){
        return _.landCards.length;
    }

    // Get total number of non-land cards in deck
    _.getNonLandCardCount = function() {
        return _.nonLandCards.length;
    }

    // TODO: get all possible commander cards in the deck
    _.getPossibleCommanderCards = function() {

        var options = [];
        
        var legendaryCards = _.getLegendaryCards();
        var legendaryCreatures = _.getCreatureCards(legendaryCards);

        legendaryCreatures.forEach(c => {
            // Do smart stuff with all Devotions
        });

        return options;
    }

    // Get all non land cards that generate mana
    _.getManaGeneratingCards = function() {
        return _.nonLandCards.filter(c => c.produced_mana.length);
    }

    // Get all non-cards that can fetch a land from the library
    _.getLandGrabCards = function() {
        var pattern = /(Search your library for).+([Basic]? Land|Island|Forest|Plains|Mountain|Swamp)/im;
        return _.getCardsByRegex(_.nonLandCards, pattern);
    }

    // Get all cards that can draw cards
    _.getDrawCards = function() {
        var pattern = /(draw[s]? (.+?) card[s]?)/im;
        return _.getCardsByRegex(_.cards, pattern);
    }

    // Helper function to get cards by using a specific RegExp
    _.getCardsByRegex = function(cards, pattern) {
        var matchedCards = [];

        cards.forEach(card => {
            var regex = new RegExp(pattern);
            var match = regex.exec(card.oracle_text);
            if(match && match.length) {
                matchedCards.push(card);
            }
        });

        return matchedCards;
    }

    // TODO : Get number of suggested amount of lands
    _.getSuggestedAmountOfLands = function (deckSize) {

        var manaGenerationCards = _.getManaGeneratingCards();
        var landGrabCards =_.getLandGrabCards();

        if (deckSize < 50) {
            // 43% optimal - take 45% and subtract landgrab/producing
            return Math.round(deckSize * 0.45) - Math.round((landGrabCards.length + manaGenerationCards.length / 5));
        } else if (deckSize < 80) {
            // 40% optimal
            return Math.round(deckSize * 0.415) - Math.round((landGrabCards.length + manaGenerationCards.length / 4));
        } else {
            // 37% optimal
            return Math.round(deckSize * 0.39) - Math.round((landGrabCards.length + manaGenerationCards.length / 4));
        }
    };

    // Initialization of analyzer with 'deck'
    init(deck);

    // Return scope
    return _;
};
