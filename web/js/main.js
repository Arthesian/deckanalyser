﻿var main = (function () {

	// Private variables
	var deckEndPoint = 'http://localhost:4004/parseDeckUrl';
	var colors = ['W', 'U', 'B', 'R', 'G']; // White, Blue, Black, Red, Green
	
	var cardData;
	var deck;

	var totalLands = 0;
	var deckCardsAmount = 0;

	var cursor = { x: 0, y: 0};

	// Initialization
	var init = function () {

		// Create the container for the floating card
		setHoverCard();

		// Bind all events
		bind();

		// Open step 1
		goToStep(1);
	}

	// Bind all click/change events
	var bind = function () {
		
		// Bind click event for the deck url input
		var submitButton = $('#deckUrlSubmitButton');
		submitButton.on('click', onParseDeckUrlClick);

		var numberOfLandsInput = $('#numberOfLands');
		numberOfLandsInput.on('change', (event) =>  {
			totalLands = +event.target.value;
			calculateLandDistribution(cardData);
			goToStep(4);
		});

		var numberOfCardsSelect = $('#deckCardCountSelect');
		numberOfCardsSelect.on('change', (event) => {

			deckCardsAmount = +event.target.value;

			var landCount = getSuggestedAmountOfLands(deckCardsAmount);
			totalLands = landCount;
			numberOfLandsInput.val(landCount);
			$('.numberOfLands').html(landCount);
			calculateLandDistribution(cardData);
			goToStep(4);
		});

		// Save the cursor location
		document.addEventListener('mousemove', (event) => {
			cursor.x = event.clientX;
			cursor.y = event.clientY;
		});
	}

	// Helper function to 'go to' a specific step in the flow
	var goToStep = function(number) {

		// Limit bounds
		if(number < 1) { number = 1 }
		if(number > 4) { number = 4 }

		// Hide all steps
		$('.step').hide();

		// Show the steps according to the selected step ( and before )
		for(var x = 1; x <= number; x++) {
			$('.step-' + x).show();
		}
	}

	// Parse the deck url on the server
	var onParseDeckUrlClick = function () {

		// Get the url that is in the input textbox
		var input = $('#deckUrlInput');
		var deckUrl = input.val();

		// Show the spinner loader
		showLoader(true);

		// Verify if there is text in the 'deckUrl'
		if(deckUrl) {

			// Do an ajax request
		    $.ajax({
				url : deckEndPoint + '?url=' + deckUrl + '&callback=?',
				method: 'GET'
		    }).done((data) => {

				console.log('Data received:', data)

				// On success, handle the data
				handleCardsResult(data);
				
		    }).fail((jqXHR, status, error) => {

				// Hide the loader on failure
				showLoader(false);

				// Show some text to notify the user of failure
				alert('Error while retrieving the deck :' + error);
			});
		}else{
			// if there is no url...
			alert('Please enter a deck url')
		}
	}

	// Helper function to show/hide the global loader
	var showLoader = function(boolean) {

		// Get the loader container
		var loader = $('#loader-container');

		// Show/hide the loader based on css class
		if(boolean) {
			loader.removeClass('hidden');
		}else{
			loader.addClass('hidden');
		}
	}

	// Handle the data from the server
	var handleCardsResult = function (data) {

		// Hide the loader on success
		showLoader(false);

		// If there is no data...
		if(!data) { console.log('No Data'); return; }
		
		// Store in scope
		cardData = data.filter(c => !!c.card); // Filter out cards without data
		cardData.forEach(d => {

			c = d.card;

			c.super_types = [];
			c.types = [];
			c.sub_types = [];

			let split = c.type_line.split(' — ');
			if(split.length == 2) { 
				// SubTypes
				let subTypes = split[1];

				c.sub_types = subTypes.split(" ");
			}

			// Check for Basic/Legendary/Snow/World/Host

			let types = split[0];
			if(types.includes('Basic')) {
				c.super_types.push('Basic')
			}

			if(types.includes('Legendary')) {
				c.super_types.push('Legendary')
			}

			if(types.includes('Snow')) {
				c.super_types.push('Snow')
			}

			if(types.includes('World')) {
				c.super_types.push('World')
			}

			if(types.includes('Host')) {
				c.super_types.push('Host')
			}

			c.types = types.split(" ").filter(t => t != "Basic" && t != "Legendary" && t != "Snow" && t != "World" && t != "Host");
		})
		
		// Move on to step 2
		goToStep(2);

		// Analyze the card data
		AnalyseResult(cardData);

		// Move to Step 3 immediately
		setSpecificDistribution();

		getSuggestedAmountOfLands(cardData.length);

		goToStep(3);
	}

	// Analyze the data from the server
	var AnalyseResult = function(data) {
		
		// Create an new deck instance based on the card data 
		deck = new Deck(data); 
		DECK = deck; // EXPOSED TO GLOBAL SCOPE ON PURPOSE

		// Set the deck overview data
		setDeckOverview();	

		// Set the deck graphs
		setDeckGraphs();
	}

	// Set all the deck graphs
	var setDeckGraphs = function() {
		// Get the data from the deck analyzer and put it into the corresponding graph
		Graphs.setCMCDistributionGraph(deck.analyser.getConvertedManaDistribution());
		Graphs.setCardTypeDistributionGraph(deck.analyser.getCardTypeDistribution());
		Graphs.setRarityDistributionGraph(deck.analyser.getRarityDistribution());
		Graphs.setColorDistributionGraph(deck.analyser.getCardColorDistribution());
		Graphs.setDevotionDistributionGraph(deck.analyser.getDevotionDistribution());
		Graphs.setLandInHandChanceDistribution(deck.analyser.getLandInHandChanceDistribution());
		Graphs.setLandManaGeneratingColorDistribution(deck.analyser.getLandManaGeneratingColorDistribution());
	}

	// Set the deck overview
	var setDeckOverview = function() {
		// Get certain type of cards
		var manaGeneratingCards = deck.analyser.getManaGeneratingCards();
		var landGrabCards = deck.analyser.getLandGrabCards();
		var legendaryCards = deck.analyser.getLegendaryCards();
		var drawCards = deck.analyser.getDrawCards();

		// 
		var list1 = createCardCollapseList("Land Grab Cards", landGrabCards);
		var list2 = createCardCollapseList("Legendary Cards", legendaryCards);
		var list3 = createCardCollapseList("Mana Producing Cards", manaGeneratingCards);
		var list4 = createCardCollapseList("Draw cards", drawCards);

		$('.landGrabCardList').empty().append(list1);
		$('.legendaryCardList').empty().append(list2);
		$('.manaProducingCardList').empty().append(list3);
		$('.drawCardList').empty().append(list4);

		$('.deckCardCount').html(deck.analyser.cards.length);
		$('.landsCardCount').html(deck.analyser.landCards.length);
	}

	// TODO :: NOT YET CALLED
	var setSpecificDistribution = function() {
		// var data = cardData.filter(d => !!d.card.colors && d.card.colors.length);

		cardData.forEach(d => {
			d.baseWeight = {};
			d.calculatedWeight = {};

			if(d.card && d.card.colors && d.card.colors.length) {
				colors.forEach(c => {
					d.baseWeight[c] = calculateBaseWeight(d.card, c) || 0;
					d.calculatedWeight[c] = d.baseWeight[c] || 0;
				});
			}
		});

		var cardStacks = splitCardsIntoStacks(cardData);

		placeCardStacks(cardStacks);

		goToStep(3);
	}

	var getSuggestedAmountOfLands = function(data) {

		return deck.analyser.getSuggestedAmountOfLands(data);
	}

	var setCalculatedWeightOfCard = function(card, multiplier) {
		var selectedCard = data.filter(d => d.card.name == card.name)[0];

		selectedCard.card.color_identity.forEach(id => {
			selectedCard.calculatedWeight[id] = selectedCard.baseWeight[id] * multiplier;
		});
	}

	// Create collapsible card list
	var createCardCollapseList = function(title, cards) {
		var box = $('<div/>').addClass('cardCollapseList');
		var header = $('<div/>').addClass('cardCollapseListTitle').html(title);

		// Click even for toggle
		header.on('click', () => {
			if(box.hasClass('open')) {
				box.removeClass('open');
			}else{
				box.addClass('open');
			}
		});

		var body = $('<div/>').addClass('cardCollapseListBody');

		cards.forEach(card => {
			var row = createCardCollapseListItem(card);

			// Add hover
			row.on('click', () => onHoverCardListItem(card));
			
			body.append(row);
		});

		box.append(header);
		box.append(body);

		return box;
	}

	// Create card row item
	var createCardCollapseListItem = function(card) {
		var body = $('<div/>').addClass('cardCollapseListItem');

		var title = $('<div/>').html(card.name);

		body.append(title);

		return body;
	}

	// Hover card
	var hoverCardContainer;
	var setHoverCard = function(card) {
		hoverCardContainer = $('<div/>').addClass('previewContainer');

		$(document.body).append(hoverCardContainer);
	}

	// Set the hover card
	var onHoverCardListItem = function(card) {

		var img = $('<img/>').addClass('previewImage').attr('src', `${card.image_uris.normal}`);

		$(img).on('click', () => {
			hoverCardContainer.empty();
			hoverCardContainer.css('display', 'none');
		});

		hoverCardContainer.css('top', cursor.y + 'px').css('left', cursor.x + 'px');
		hoverCardContainer.empty().append(img);
		hoverCardContainer.css('display', 'block');
	}

	var calculateLandDistribution = function(data) {

		if(!data) { console.log('No cards loaded!'); return; }

		var coloredWeights = { 'W' : 0, 'U' : 0, 'B' : 0, 'R' : 0, 'G' : 0, 'C' : 0 };
		var weightSum = 0;

		data.forEach(d => {
			d.card.color_identity.forEach(id => {
				coloredWeights[id] += d.calculatedWeight[id] || 0;
				weightSum += d.calculatedWeight[id] || 0;
			});
		});

		const lands = { 
			plains : Math.round((coloredWeights.W / weightSum) * totalLands),
			islands : Math.round((coloredWeights.U / weightSum) * totalLands),
			swamps : Math.round((coloredWeights.B / weightSum) * totalLands),
			mountains : Math.round((coloredWeights.R / weightSum) * totalLands),
			forests : Math.round((coloredWeights.G / weightSum) * totalLands),
			deserts : Math.round((coloredWeights.C / weightSum) * totalLands)
		}

		const sum = Object.values(lands).reduce((total, val) => total + val, 0);
		if(sum != totalLands) {
			// get max land amount
			const name = Object.entries(lands).reduce((max, entry) => max[1] > entry[1] ? max : entry)[0];

			if(sum < totalLands) {
				// add one
				lands[name]++;
			} else {
				// reduce one
				lands[name]--;
			}
		}

		$('#PlainsAmount').html(lands.plains);
		$('#IslandAmount').html(lands.islands);
		$('#SwampAmount').html(lands.swamps);
		$('#MountainAmount').html(lands.mountains);
		$('#ForestAmount').html(lands.forests);
		$('#DesertAmount').html(lands.deserts);
	}

	var splitCardsIntoStacks = function (cards) {

		data = cards;

		cardStacks = [];

		var multiColorCards = data.filter(d => d.card.colors.length > 1);
		var whiteCards = data.filter(d => d.card.colors.length == 1 && d.card.colors.includes('W'));
		var blueCards = data.filter(d => d.card.colors.length == 1 && d.card.colors.includes('U'));
		var blackCards = data.filter(d => d.card.colors.length == 1 && d.card.colors.includes('B'));
		var redCards = data.filter(d => d.card.colors.length == 1 && d.card.colors.includes('R'));
		var greenCards = data.filter(d => d.card.colors.length == 1 && d.card.colors.includes('G'));
		var colorlessCard = data.filter(d => d.card.colors.length == 0 && !d.card.types.includes("Land"));

		if (multiColorCards.length) {
			cardStacks.push({
				cards: multiColorCards
			});
		}
		if (whiteCards.length) {
			cardStacks.push({
				cards: whiteCards
			});
		}
		if (blueCards.length) {
			cardStacks.push({
				cards: blueCards
			});
		}
		if (blackCards.length) {
			cardStacks.push({
				cards: blackCards
			});
		}
		if (redCards.length) {
			cardStacks.push({
				cards: redCards
			});
		}
		if (greenCards.length) {
			cardStacks.push({
				cards: greenCards
			});
		}
		if (colorlessCard.length) {
			cardStacks.push({
				cards: colorlessCard
			});
		}

		return cardStacks;
	}

	var calculateBaseWeight = function (card, color) {
		var colorAmount = 0;
		var colorlessAmount = 0;
		
		if(card.mana_cost){
			colorAmount = (card.mana_cost.split(color).length - 1);
			colorlessAmount = +card.mana_cost.split('}')[0].replace('{', '');
			colorlessAmount = colorlessAmount || 0;
		}

		return colorAmount / (colorlessAmount + 1);
	}

	var placeCardStacks = function () {
		var resultContainer = $('#cardList');
		resultContainer.empty();

		var leftContainer = $('<div/>').addClass('col col-12 col-lg-6 left');
		var rightContainer = $('<div/>').addClass('col col-12 col-lg-6 right');

		var leftCards = 0;
		var rightCards = 0;

		var placeLeft = true;

		cardStacks.sort((a, b) => b.cards.length - a.cards.length);

		cardStacks.forEach((stack, i, stacks) => {

			var cardColumn = createCardColumn(stack, stacks.length);

			placeLeft = (leftCards <= rightCards);

			if (placeLeft) {
				leftCards += cardColumn.cards;
				leftContainer.append(cardColumn);
			} else {
				rightCards += cardColumn.cards;
				rightContainer.append(cardColumn);
			}

			resultContainer.append(leftContainer);
			resultContainer.append(rightContainer);
		})
	}

	var createCardColumn = function (stack, columns) {

		columns = columns || 1;

		var column = $('<div/>').addClass('card-column step step-2');

		var cards = stack.cards;

		var color = cards[0].card.colors[0];
		var colorId = cards[0].card.color_identity[0];

		cards.sort((a, b) => {
			return b.baseWeight[colorId] - a.baseWeight[colorId]
		});

		var columnTitle = color;
		if(cards[0].card.colors.length > 1) {
			columnTitle = 'Multicolor';
		}

		if(!cards[0].card.colors.length) {
			columnTitle = 'Colorless';
		}

		var title = $('<h2>').append(columnTitle);

		column.append(title);

		cards.forEach((card, i) => {
			var cardTile = createCardTile(card)

			column.append(cardTile);
		});

		return column;
	}

	var createCardTile = function (cardObject) {
		var tile = $('<div/>').addClass('card-tile col col-12');

		var tileRow = $('<div/>').addClass('row');

		var amountBox = $('<div/>').addClass('col col-1');
		var nameBox = $('<div/>').addClass('col col-5');
		var costBox = $('<div/>').addClass('col col-2');
		var weightAdjustBox = $('<div/>').addClass('col col-3');
		var weightBox = $('<div/>').addClass('col col-1');

		var card = cardObject.card;

		amountBox.append(cardObject.amount);

		nameBox.append(card.name);

		var baseWeight = round(cardObject.baseWeight[card.color_identity[0]], 2); // Simplistic >.> -- no multi color

		costBox.append(parseManaSymbolsToImages(card.mana_cost));

		var weightInput = $('<input>').attr('type', 'range').attr('step', '0.1').attr('min', '0').attr('max', '2').val('1');
		weightInput.on('input', () => {
			var calcWeight = round(weightInput.val() * baseWeight, 2);
			
			setCalculatedWeightOfCard(cardObject, +weightInput.val());

			weightBox.html(calcWeight);

			if(totalLands) {
				calculateLandDistribution(cardData);
			}
		});

		weightAdjustBox.append(weightInput);

		weightBox.append(baseWeight);

		tileRow.append(amountBox);
		tileRow.append(nameBox);
		tileRow.append(costBox);
		tileRow.append(weightAdjustBox);
		tileRow.append(weightBox);

		tile.append(tileRow);

		return tile;
	}

	var round = function (number, decimals) {
		if(!number) return 0;
		return +number.toFixed(decimals || 0);
	}

	var parseManaSymbolsToImages = function (manaCost) {
		var costBox = $('<div/>');

		var split = manaCost ? manaCost.split('{') : ['0'];

		split.forEach((val) => {
			val = val.replace('}', '');

			if (!val) {
				return;
			}

			val = val.replace('/', '-');

			var manaContainer = $('<div/>').addClass('mana mana-' + val);
			costBox.append(manaContainer);
		});

		return costBox;
	}

	// Initialization
	init();

})();