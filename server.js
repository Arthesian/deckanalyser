// MongoDB login credentials
const username = 'arthesian';
const password = 'Zto6uI3nCIagJHlHOLSc';

// Server variables 
const express = require('express'),
  app = express(),
  port = process.env.PORT || 4004;
  mongoose = require('mongoose'),
  Task = require('./api/models/CardModel'), //created model loading here
  bodyParser = require('body-parser'),
  cors = require('cors');

  mongoose.Promise = global.Promise;
  
  // Connect to database
  mongoose.connect(`mongodb+srv://${username}:${password}@cluster0-hemmp.mongodb.net/Cluster0?authSource=admin`, { useNewUrlParser: true }); 
  
  app.use(cors());

  // JSON output handler
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  
  var deckRoutes = require('./api/routes/DeckRoutes'); //importing route
  deckRoutes(app); //register the route

  var dbRoutes = require('./api/routes/DBRoutes');
  dbRoutes(app);

app.listen(port);

console.log('Deck Analyzer server started on port: ' + port);