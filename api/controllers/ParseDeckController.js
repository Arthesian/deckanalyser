var mongoose = require('mongoose');
var request = require('request');
var cheerio = require('cheerio');
var Card = mongoose.model('card');

// Real function
exports.get_cards_from_deck_url = function(req, res) {
  var url = req.query.url;
  
  var cards = [];

  request(url, (error, response, html) => {

    if(!error && response.statusCode == 200) {
    
      if(url.indexOf('bazaarofmagic') > -1) {
        console.log(`Bazaar of Magic URL detected...`)
        cards = parseCardsFromBazaarOfMagic(html);
      }
      
      if(url.indexOf('tappedout') > -1) {
        console.log(`Tapped Out URL detected...`)
        cards = parseCardsFromTappedOut(html);
      }
      
      if(url.indexOf('scryfall') > -1) {
        console.log(`Scryfall URL detected...`)
        cards = parseCardsFromScryfall(html);
      }

      console.log(`Parsed ${cards.length} Cards...`);
      
      var promises = [];
      
      cards.forEach((c) => {
        promises.push(retrieveCardByName(c.name));
      });
      
      Promise.all(promises).then((data) => {
        
        console.log(`Loaded ${data.length} Cards...`);

        // Match the card data with the parsed amount
        cards.forEach((card, i) => {
            card.card = data[i]; // TODO: check if this is correct
        });

        console.log(`Matched all card data to `)

        res.json(cards);

      }).catch(err => {

        console.error(err);

        res.send(err);
      });

    }else{
      res.json({ error : error, code : response && response.statusCode });
    }
  });
}

var parseCardsFromBazaarOfMagic = function(html) {

  var cards = [];

  var $ = cheerio.load(html, null, false);

  var page = $('.column.page');

  var cardLists = page.find('ul.no-bullet');

  cardLists.each((i, list) => {

    var listName = $(list).parent().children()[0].childNodes[0].data;

    if(!listName) { return; }

    listName = listName.toLowerCase();

    // Skip lands & sideboard
    if(listName.indexOf('sideboard') > -1) {
      return;
    }

    var items = $(list).find('li');

    items.each((i, li) => {

      // Get number of times in deck
      var amount = +li.childNodes[0].data.slice(0,-2);
      
      // Get name of the card
      var name = li.childNodes[1].childNodes[0].data;

      // Push to cards array
      cards.push({name : name, amount : amount});
    });
  });

  return cards;

}

var parseCardsFromTappedOut = function(html) {
  // TODO
  var cards = [];

  var $ = cheerio.load(html, null, false);

  var board = $('.row.board-container');

  var cardLists = $(board[0]).find('.boardlist');

  cardLists.each((li, list) => {

    var listName = $(list).parent().children()[0].childNodes[0].data;

    if(!listName) { return; }

    listName = listName.toLowerCase();

    var items = $(list).find('.member');

    items.each((i, item) => {

        var links = $(item).find('a');

        var amount = links[0].childNodes[0].data.slice(0,-1);
        var name = links[1].childNodes[0].data;

        cards.push({name : name, amount : amount});
    });
  });

  return cards;
}

var parseCardsFromScryfall = function(html) {
  var cards = [];

  var $ = cheerio.load(html, null, false);

  var items = $('.deck-list-entry');

  items.each((i, item) => {

      var links = $(item).find('a:not([class*="currency"])');

      var amount = links[0].childNodes[0].data;
      var name = links[1].childNodes[0].data;

      cards.push({name : name.trim(), amount : +amount});
  });

  return cards;
}

var retrieveCardByName = function(name) {

  if(name.includes("//")) {
    name = name.split("//")[0].trim();
  }

  return Card.findOne({ name : name, rarity : { $ne: 'Special' } }, null, { sort: { released_at : -1 }});
  //, { sort : { multiverseid : -1 } }
}

exports.get_a_card_by_id = function(req, res) {
  Card.find({ MultiverseId : +req.params.multiverseId }).exec(function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.get_a_card_by_name = function(req, res) {
  Card.find({ name : req.params.name, multiverseid : { $ne : null } }).sort({ multiverseid : -1 }).limit(1).exec(function(err, task) {
    if (err)
      res.send(err);
    res.json(task[0]);
  });
};

var static_json = ``;