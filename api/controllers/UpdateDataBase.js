var mongoose = require('mongoose');
var loadJsonFile = require('load-json-file');
var fs = require('fs')

var simpleCard = mongoose.model('simplecard');
var uniqueCard = mongoose.model('uniquecard');
var fullCard = mongoose.model('card');

exports.delete_all_full_cards = async (req, res) => {
    // Clean DB
    await fullCard.remove({});
        
    // Send Response
    res.json({ message : 'Cleared DB of FullCards!'})
}

        var cards = [];

        Object.keys(json).forEach((key) => {

            var card = json[key];

            cards.push(card);

        });

        // Clean DB
        simpleCard.remove({}).then(() => {
            // Fill DB
            simpleCard.insertMany(cards).then((result) => {
                res.json({ message : 'success!'})
            });
        })


    }).catch(err => {
        res.json({message : 'Someting went wrong while parsing the local JSON DB file', error : err})
    });
}

function getMTGJsonCardsFromSets(json) {
    
    var cards = {};
    
    Object.keys(json).forEach((key) => {

        var set = json[key];

        set.cards.forEach((card) => {
            card.releaseDate = set.releaseDate;
            cards[card.multiverseid] = card;
        });

    });

    return cards;
}

exports.update_scryfallcards_db_from_file = function(req, res) {
    loadJsonFile('api/json/scryfall-oracle-cards.json'). then(SFjson => {
        var cards = [];

        var start = Date.now();

        SFjson.forEach(card => {

            card.multiverseid = Math.max.apply(null, card.multiverse_ids);
            if(!Number.isFinite(card.multiverseid) || card.multiverseid < 1 ) { return; }
            
            delete card.multiverse_ids;
            delete card.object;
            delete card.id;
            delete card.oracle_id;
            delete card.scryfall_uri;
            delete card.nonfoil;
            delete card.oversized;
            delete card.scryfall_set_uri;
            delete card.illustration_id;
            delete card.edhrec_rank;

            cards.push(card);
        });

        var sfjsonTime = Date.now();
 
        loadJsonFile('api/json/AllSets-x.json').then(json => {
            var MTGJsonCards = getMTGJsonCardsFromSets(json);
            var mtgjsonTime = Date.now();

            cards.forEach(c => {
                let mtgCard = MTGJsonCards[c.multiverseid];

                if(mtgCard) {
                    c.subtypes = mtgCard.subtypes;
                    c.types = mtgCard.types;
                    c.supertypes = mtgCard.supertypes;
                    c.rulings = mtgCard.rulings;
                    c.original_text = mtgCard.originalText;
                    c.original_type = mtgCard.originalType;
                    c.colors = mtgCard.colors;
                    c.release_date = mtgCard.releaseDate;
                    c.rarity = mtgCard.rarity;
                }
            });

            var mergeTime = Date.now();

            // Clean DB
            uniqueCard.remove({}).then(() => {
                // Fill DB
                uniqueCard.insertMany(cards).then((result) => {
                    res.json({ 
                        message : 'success!',
                        performance : {
                            sfjsonloop : sfjsonTime - start,
                            mtgjsonloop : mtgjsonTime - sfjsonTime,
                            merge : mergeTime - mtgjsonTime
                        }
                    })
                });
            })

        });        
    });
}

exports.update_fullcard_db_from_file = function(req, res) {
    loadJsonFile('api/json/AllSets-x.json').then(json => {
        
        var cards = getMTGJsonCardsFromSets(json);

        // Clean DB
        fullCard.remove({}).then(() => {
            // Fill DB
            fullCard.insertMany(cards).then((result) => {
                res.json({ message : 'success!'})
            });
        })

    }).catch(err => {
        res.json({message : 'Someting went wrong while parsing the local JSON DB file', error : err})
    });
}