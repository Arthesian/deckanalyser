var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// var CompleteCard = new Schema({
//     artist : String,
//     asciiName : String,
//     borderColor : String,
//     colorIdentity : Array,
//     colors : Array,
//     convertedManaCost : Number,               // ConvertedManaCost
//     flavourText : String,           // FlavourText
//     hasAlternativeDeckLimit : Boolean,
//     identifiers : Object,         // replaced by : Identifiers //
//     isAlternative : Boolean,
//     keywords: Array,
//     legalities : Array,
//     manaCost : String,
//     name : String,
//     number : String,
//     originalText : String,
//     originalType : String,
//     power : String,
//     printings : Array,
//     rarity : String,
//     rulings : Array,
//     setCode : String,
//     subtypes : Array,
//     supertypes : Array,
//     text : String,
//     toughness : String,
//     type : String,
//     types : Array,
//     uuid : String,
//     variations : Array
// });

var CompleteCard = new Schema({
    artist: String,
    cmc: Number,
    colorIdentity: Array,
    colors: Array,
    flavour: String,
    imageName: String,
    layout: String,
    legalities: Array,
    manaCost: String,
    mciNumber: String,
    multiverseid: Number,
    name: String,
    number: String,
    originalText: String,
    originalType: String,
    power: String,
    printings: Array,
    rarity: String,
    rulings: Array,
    subtypes: Array,
    supertypes: Array,
    text: String,
    toughness: String,
    type: String,
    types: Array
});

module.exports = mongoose.model('card', CompleteCard, 'cards');

// var SimpleCard = new Schema({
//     layout : String,
//     name : String,
//     manaCost : String,
//     cmc : Number,
//     colors : Array,
//     type : String,
//     types : Array,
//     subtypes : Array,
//     text : String,
//     power : String,
//     toughness : String,
//     imageName : String,
//     colorIdentity : Array
// });

var SimpleCard = new Schema({
    layout: String,
    name: String,
    manaCost: String,
    cmc: Number,
    colors: Array,
    type: String,
    types: Array,
    subtypes: Array,
    text: String,
    power: String,
    toughness: String,
    imageName: String,
    colorIdentity: Array
});

module.exports = mongoose.model('simplecard', SimpleCard, 'cards_simple');

var UniqueCard = new Schema({
    multiverseid: Number,
    name: String,
    lang: String,
    uri: String,
    layout: String,
    highres_image: Boolean,
    image_uris: Object,
    mana_cost: String,
    cmc: Number,
    type_line: String,
    oracle_text: String,
    colors: Array,
    color_identity: Array,
    legalities: Object,
    reserved: Boolean,
    foil: Boolean,
    oversized: Boolean,
    reprint: Boolean,
    set: String,
    set_name: String,
    set_uri: String,
    set_search_uri: String,
    rulings_uri: String,
    prints_search_uri: String,
    collector_number: String,
    digital: Boolean,
    rarity: String,
    artist: String,
    frame: String,
    full_art: Boolean,
    border_color: String,
    timeshifted: Boolean,
    colorshifted: Boolean,
    futureshifted: Boolean,
    related_uris: Object,
    releasedate: String,
    subtypes : Array,
    supertypes : Array,
    types: Array,
    rulings: Array,
    original_text : String,
    original_type: String, 
});

module.exports = mongoose.model('uniquecard', UniqueCard, 'cards_unique');