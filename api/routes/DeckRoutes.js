'use strict';
module.exports = function(app) {
  var todoList = require('../controllers/ParseDeckController');

  // todoList Routes
  app.route('/cardByName/:name')
    .get(todoList.get_a_card_by_name);

  app.route('/cardById/:multiverseId')
    .get(todoList.get_a_card_by_id);
  
  app.route('/parseDeckUrl/')
    .get(todoList.get_cards_from_deck_url);
};