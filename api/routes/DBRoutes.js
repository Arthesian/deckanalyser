'use strict';
module.exports = function(app) {
  var dbController = require('../controllers/UpdateDataBase');

  // update Simple card DB
  app.route('/updatedbsimple/')
    .get(dbController.update_simplecard_db_from_file);
  // update Full card DB
  app.route('/updatedbfull/')
    .get(dbController.update_fullcard_db_from_file);
  // update Full card DB
  app.route('/updatedbunique/')
    .get(dbController.update_scryfallcards_db_from_file);
};